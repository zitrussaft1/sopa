# SOPA


<div align="center">
<img alt="Sopa logo" src="images/logo.png" width="300"/>
</div>

## SOlarus PAckage Manager

The objective of SOPA is to easily allow redistribution of Solarus game assets, its main functions are:

* Downloading: Retrieving files from an external source
* Patching: Modifying files to work with different projects
* Integrating: Allowing developers to use functions/entities from other projects

## Getting Started

Download sopa:

[Linux](target/debug/sopa)

[Windows](result/bin/soul.exe)

To use some other project's files you should create a `sopa.toml` file in the same directory where the `data` folder is located. The file should contain a `[dependencies]` header followed by all projects you wish to clone and their identifiers.

```toml
[dependencies]
trillium = "gitlab:zitrussaft1/trillium"
# or
trillium = { src="gitlab:zitrussaft1/trillium" }
```

Then call sopa!
```bash
$ sopa install
```

## Usage

Entities and other assets can simply be added through Solarus Quest Editor.

![Quest Editor](images/qe.png)

To use functions created by a library, for example `camera:shake` from trillium, you can call the package manager, tell it to use foo library then tell it to stop using it.

```lua
local pm = require("scripts/meta/master")
local map = ...

local function some_function()
  pm:set("trillium", "map")
  map:screenshake()
  pm:set("default", "map")
end
```

## Library limitations

There are a few things that are awaited by this package manager when patching files, these are: metatables paths and metatables events.

These should be only relevant to SOPA library developers, but endusers experimenting with SOPA might find it useful.

### Metatable Paths

Scripts that make use of `sol.main.get_metatable(...)` should be placed under `scripts/meta` in the library quest. This is because the path is the sole factor for the package manager to append the values to the `master.lua` script.

### Metatable events

Files inside `scripts/meta` should not contain `:register_event` functions or similar, these will cause errors when running the game.

It should be noted that hopefully one day these will work as intended, but currently they don't.

## This project is WIP

The development of SOPA comes from a want to make Solarus more accessible and mainstream, however its development is limited by the time I have. Features will be added as I see them needed for the game we (Zitrussaft) are currently developing. That said, my hope for SOPA is for it to eventually become a full-featured and accessible asset store for Solarus with a GUI and everything, let's only hope.

To-do list:

- [ ] `add, remove, update` commands to SOPA
- [ ] File removal when `sopa.toml` is updated
- [ ] Lock file with individual file-hashing to prevent unwanted deletions
- [ ] Allow libraries to specify which files should be served
- [ ] Allow end-users to choose which files to download/use
- [ ] Create more examples of packages
- [ ] Improve `luarocks` support
- [ ] Clean up and offer better download links

To-dream list:

- [ ] Automagically update `project_db.dat`
- [ ] Have a platform to find libraries
- [ ] Asset Store