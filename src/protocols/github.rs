use regex::Regex;
use serde_json::{Value};

use reqwest::{
    header::{HeaderMap, ACCEPT, USER_AGENT},
    Url,
};

fn construct_header() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(USER_AGENT, "User".parse().unwrap());
    headers.insert(ACCEPT, "application/vnd.github.v3+json".parse().unwrap());
    return headers;
}

fn get(url: &str) -> Result<reqwest::blocking::Response, reqwest::Error> {
    let url = Url::parse(&url).unwrap();
    let client = reqwest::blocking::Client::builder()
        .default_headers(construct_header())
        .build()?;
    let response = client.get(url).send()?;
    return Ok(response);
}

// TODO (low priority): Find if host works
// TODO: Add bitbucket support (maybe)
// https://developer.atlassian.com/cloud/bitbucket/rest/api-group-refs/#api-repositories-workspace-repo-slug-refs-get
// https://stackoverflow.com/questions/13044749/bitbucket-download-source-as-zip
macro_rules! api_functions {
    ($api:ty, $durl:expr, $ref:expr, $ref_host:expr, $tar:expr, $tar_host:expr, $dref:expr, $dref_host:expr) => {
        ::paste::paste! { 
            fn [<get_ $api _rev_from_ref>](owner: &str, repo: &str, ref_: &str, host: &str) -> String {
                let url: String = {
                    if host == $durl {
                        format!($ref, owner, repo, ref_)
                    } else {
                        format!($ref_host, host, owner, repo, ref_)
                    }
                };
            
                let response = get(&url).unwrap();
                let json: Value = response.json().unwrap();
                if $durl == "github.com" {
                    let rev = json["sha"].as_str().unwrap_or("");
                    rev.to_string()
                } else if $durl == "gitlab.com" {
                    let rev = json["id"].as_str().unwrap_or("");
                    rev.to_string()
                } else {
                    "".to_string()
                }
            }
            
            fn [<get_ $api _download_url>](owner: &str, repo: &str, rev: &str, host: &str) -> String {
                if host == $durl {
                    format!($tar, owner, repo, rev)
                } else {
                    format!($tar_host, host, owner, repo, rev)
                }
            }
            
            fn [<get_ $api _rev_from_default>](owner: &str, repo: &str, host: &str) -> String {
                let url: String = {
                    if host == $durl {
                        format!($dref, owner, repo)
                    } else {
                        format!($dref_host, host, owner, repo)
                    }
                };
                let response = get(&url).unwrap();
                let json: Value = response.json().unwrap();
                if $durl == "github.com" {
                    let rev = json[0]["sha"].as_str().unwrap_or("");
                    rev.to_string()
                } else if $durl == "gitlab.com" {
                    let rev = json[0]["id"].as_str().unwrap_or("");
                    rev.to_string()
                } else {
                    "".to_string()
                }
            }
        }
    }
}

api_functions!(github, "github.com", 
    "https://api.github.com/repos/{}/{}/commits/{}", "https://{}/api/v3/repos/{}/{}/commits/{}",
    "https://api.github.com/repos/{}/{}/tarball/{}", "https://{}/api/v3/repos/{}/{}/tarball/{}",
    "https://api.github.com/repos/{}/{}/commits", "https://{}/api/v3/repos/{}/{}/commits"
);
api_functions!(gitlab, "gitlab.com", 
    "https://gitlab.com/api/v4/projects/{}%2F{}/repository/commits/{}", "https://{}/api/v4/projects/{}%2F{}/repository/commits/{}",
    "https://gitlab.com/api/v4/projects/{}%2F{}/repository/archive.tar.gz?sha={}", "https://{}/api/v4/projects/{}%2F{}/repository/archive.tar.gz?sha={}",
    "https://gitlab.com/api/v4/projects/{}%2F{}/repository/commits", "https://{}/api/v4/projects/{}%2F{}/repository/commits"
);

fn get_rev_from_ref(protocol: &str, owner: &str, repo: &str, ref_: &str, host: &str) -> String {
    if protocol == "github" {
        get_github_rev_from_ref(owner, repo, ref_, host)
    } else if protocol == "gitlab" {
        get_gitlab_rev_from_ref(owner, repo, ref_, host)
    } else {
        return "".to_string();
    }
}

fn get_download_url(protocol: &str, owner: &str, repo: &str, rev: &str, host: &str) -> String {
    if protocol == "github" {
        get_github_download_url(owner, repo, rev, host)
    } else if protocol == "gitlab" {
        get_gitlab_download_url(owner, repo, rev, host)
    } else {
        return "".to_string();
    }
}

fn get_rev_from_default(protocol: &str, owner: &str, repo: &str, host: &str) -> String {
    if protocol == "github" {
        get_github_rev_from_default(owner, repo, host)
    } else if protocol == "gitlab" {
        get_gitlab_rev_from_default(owner, repo, host)
    } else {
        return "".to_string();
    }
}

struct Query {
    rev: Option<String>,
    ref_: Option<String>,
    host: Option<String>,
}
pub fn parse_github_string(repo_string: &str) -> Result<String, String> {
    // Split into protocol and url
    let (url, query) = repo_string.split_once('?').unwrap_or((repo_string, ""));

    let mut split = url.split(':');
    let protocol = split.next().ok_or("No protocol provided".to_string())?;
    let url = split.next().ok_or("No url provided".to_string())?;

    // Collect each place as a list
    let place = url.split('/').collect::<Vec<&str>>();

    // Get host, rev and ref from query
    let query_list = query.split('&');
    let mut query = Query {
        rev: None,
        ref_: None,
        host: None,
    };

    for param in query_list {
        if let Some((key, value)) = param.split_once('=') {
            match key {
                "ref" => query.ref_ = Some(value.to_string()),
                "rev" => query.rev = Some(value.to_string()),
                "host" => query.host = Some(value.to_string()),
                _ => {}
            }
        }
    }
    
    let re_rev = Regex::new(r"[0-9a-fA-F]{40}").unwrap();
    let re_ref_ =  Regex::new(r"[a-zA-Z0-9@][a-zA-Z0-9_.\\/@-]*").unwrap();

    let host = query.host.unwrap_or(format!("{}.com", protocol));
    let rev = {
        if let Some(ref ref_) = query.ref_ {
            if !re_ref_.is_match(&ref_) {
                return Err("Not a valid branch name".to_string());
            }
            get_rev_from_ref(protocol, place[0], place[1], &ref_, &host)
        } else if let Some(ref rev) = query.rev {
            if !re_rev.is_match(&rev) {
                return Err("Not a valid commit hash".to_string());
            }
            rev.to_string()
        } else {
            get_rev_from_default(protocol, place[0], place[1], &host)
        }
    };

    let url: String = {
        if place.len() == 3 {
            if re_rev.is_match(place[2]) {
                if query.rev.is_some() {
                    return Err("Rev was provided in query, but also in url".to_string());
                }
                get_rev_from_ref(protocol, place[0], place[1], place[2], &host)
            } else if re_ref_.is_match(place[2]) {
                if query.ref_.is_some() {
                    return Err("Ref was provided in query, but also in url".to_string());
                }
                let rev = get_rev_from_ref(protocol, place[0], place[1], place[2], &host);
                get_download_url(protocol, place[0], place[1], &rev, &host)
            } else {
                return Err("Invalid url format".to_string());
            }
        } else if place.len() == 2 {
            get_download_url(protocol, place[0], place[1], &rev, &host)
        } else {
            return Err("Invalid url format".to_string());
        }
    };

    Ok(url)

}