use regex::Regex;

// TODO: Read environment variables and set --lua-dir and luarocks path
pub fn parse_luarocks_string(repo_string: &str) -> Result<String, String> {
    let re = Regex::new(r#"^luarocks:(?:"([^"]*)")?\/?([^"\/]+)(?:\/([^"]+))?"#).unwrap();
    if let Some(captures) = re.captures(repo_string) {
        let repository_url = captures.get(1).map_or("luarocks.org", |m| m.as_str());
        let package_name = captures.get(2).unwrap().as_str();
        let version = captures.get(3).map_or("", |m| m.as_str());

        let command = format!("luarocks --local --tree=./local install --server {} {} {}", repository_url, package_name, version);
        Ok(command)
    } else {
        Err("Not a valid luarocks url".to_string())
    }
}