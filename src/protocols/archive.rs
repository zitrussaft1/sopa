use regex::Regex;

pub fn parse_http_repo_string(repo_string: &str) -> Result<String, String> {
    let url = repo_string.split_once(":").ok_or("No url provided".to_string())?.1;
    let re_http = Regex::new(r"^https?://").unwrap();
    if !re_http.is_match(url) {
        return Err("Not a valid http(s) url".to_string());
    }
    Ok(url.to_string())
}