#![feature(let_chains)]

mod protocols {
    pub mod github;
    pub mod archive;
    pub mod luarocks;
}

mod helpers;

pub use protocols::github::parse_github_string;
pub use protocols::archive::parse_http_repo_string;
pub use protocols::luarocks::parse_luarocks_string;

pub use helpers::{
    download_and_extract,
    get_files,
    get_methods_and_deps,
    add_deps_to_file,
    get_file_blake3,
    generate_master_script,
    get_new_path,
};

use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::collections::HashMap;
use std::thread;

use pathbuf_ext::PathExt;
use indicatif::{ProgressBar, ProgressStyle, MultiProgress};
use toml::Value;
use clap::Parser;


fn parse_repo_string(repo_string: &str) -> String {
    let protocol = repo_string.split(':').next().unwrap_or("");

    match protocol {
        // Gitlab or github
        "github" | "gitlab" => parse_github_string(repo_string).unwrap(),
        "archive" => parse_http_repo_string(repo_string).unwrap(),
        // Luarocks
        "luarocks" => parse_luarocks_string(repo_string).unwrap(),
        // TODO: Add git protocol
        // "git" => parse_git_repo_string(repo_string),
        _ => "".to_string()
    }
}

pub fn build_with_ui(lib_name: String, src: &PathBuf, dest: &PathBuf, multi: &MultiProgress) -> Result<(), Box<dyn std::error::Error>> {

    let mut files: HashMap<String, Vec<String>> = HashMap::new();
    let mut files_vec: Vec<PathBuf> = Vec::new();
    get_files(src, &mut files_vec)?;

    // Eventually add a lock file with the blake3 hashes of each file
    //let mut blake3_file = File::create("blake3")?;

    let progress = multi.add(ProgressBar::new(files_vec.len() as u64));
    
    progress.set_style(ProgressStyle::default_bar()
        .template(&(lib_name.to_owned() + " (finding deps): [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})"))
        .unwrap()
        .progress_chars("#>-"));

    for file in files_vec.clone() {
        if file.starts_with(&(src.join("scripts/meta/"))) && file.is_file() {
            let methods_from_file = get_methods_and_deps(&file);
            // Get the file name without the extension
            let file_name = file.file_name().unwrap().to_str().unwrap().to_string().replace(".lua", "");
            files.insert(file_name, methods_from_file.keys().map(|x| x.to_string()).collect::<Vec<String>>());
        }
        progress.inc(1);
    }
    progress.finish_with_message(lib_name.to_owned() + ": Done getting methods and dependencies from scripts/meta!");

    let progress = multi.insert_after(&progress, ProgressBar::new(files_vec.len() as u64));
    progress.set_message(lib_name.to_owned() + ": Adding dependencies to scripts...");
    progress.set_style(ProgressStyle::default_bar()
        .template(&(lib_name.to_owned() + " (adding deps): [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})"))
        .unwrap()
        .progress_chars("#>-"));
    for file in &files_vec {
        if 
            file.is_file() && 
            file.extension().unwrap_or_default() == "lua" ||
            file.extension().unwrap_or_default() == "dat"
         {
            let methods_from_file = get_methods_and_deps(&file);
            add_deps_to_file(&file, &methods_from_file, &files, lib_name.clone())?;

            //let blake3 = get_file_blake3(file)?;
            //blake3_file.write_all((
            //    file.replace(
            //        src.to_str().unwrap(),
            //        dest.to_str().unwrap()
            //    ).to_str().unwrap().to_string() +
            //    " " + &blake3 + "\n").as_bytes())?;
        }
        
        progress.inc(1);
    }
    progress.finish_with_message(lib_name.to_owned() + ": Done adding dependencies to scripts!");

    let mut paths: Vec<PathBuf> = Vec::new();
    get_files(src, &mut paths)?;

    let progress = multi.insert_after(&progress, ProgressBar::new(files_vec.len() as u64));
    progress.set_style(ProgressStyle::default_bar()
        .template(&(lib_name.to_owned() + " (copying files): [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})"))
        .unwrap()
        .progress_chars("#>-"));

    for path in paths {
        let new_path: PathBuf = {
            let path = path.replace(src.to_str().unwrap(), dest.to_str().unwrap());
            get_new_path(&path, &lib_name)
        };
        
        let parent_dir = Path::new(&new_path).parent().unwrap();
        if !parent_dir.exists() {
            fs::create_dir_all(parent_dir)?;
        }
        fs::copy(path, new_path)?;
        progress.inc(1);
    }
    progress.finish_with_message(lib_name.to_owned() + ": Done copying files!");
    Ok(())
}

fn parse_toml_file(file_path: &str) -> Result<HashMap<String, PathBuf>, Box<dyn std::error::Error>> {
    // Read the contents of the TOML file
    let toml_content = fs::read_to_string(file_path)?;

    // Parse the TOML content
    let parsed_toml: Value = toml::from_str(&toml_content)?;

    let mut folders: HashMap<String, PathBuf> = HashMap::new();


    // Access the dependencies section
    if let Some(dependencies) = parsed_toml.get("dependencies") {
        if let Value::Table(dependencies_table) = dependencies {
            for (dep_name, dep_value) in dependencies_table.iter() {
                let download_path = std::env::temp_dir().join(dep_name);
                match dep_value {
                    Value::String(line) => {
                        download_and_extract(
                            &parse_repo_string(line),
                            download_path.clone().to_str().unwrap().to_string()
                        )?;
                        folders.insert(
                            dep_name.to_string(),
                            download_path.clone()
                        );
                    }
                    Value::Table(table) => {
                        if let Some(src) = table.get("src") {
                            if let Value::String(src_value) = src {
                                download_and_extract(
                                    &parse_repo_string(src_value), 
                                    download_path.clone().to_str().unwrap().to_string()
                                )?;
                                folders.insert(
                                    dep_name.to_string(),
                                    download_path.clone()
                                );
                            }
                        }
                    }
                    _ => {
                        println!("Invalid format for dependency {}", dep_name);
                    }
                }
            }
        }
    }

    Ok(folders)
}


fn install() {
    //let guard = pprof::ProfilerGuardBuilder::default().frequency(1000).blocklist(&["libc", "libgcc", "pthread", "vdso"]).build().unwrap();
    let multi = MultiProgress::new();
    let deps = parse_toml_file("sopa.toml").unwrap();
    // Using multiple threads to build dependencies
    let mut handles = Vec::new();
    for (dep_name, dep_path) in deps.clone() {
        let multi = multi.clone();
        let handle = thread::spawn(move || {
            build_with_ui(dep_name, &dep_path, &PathBuf::from("data"), &multi).unwrap();
            // Remove the folder after building
            fs::remove_dir_all(dep_path).unwrap();
            //println!("Finished building {:?}", dep_path);
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().expect("Failed to join thread");
    }
    generate_master_script(
        &PathBuf::from("data"),
        &PathBuf::from("data"),
    ).unwrap();
    println!("Dependencies added: {}", deps.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(", "));
    println!("Installation completed successfully!");

    //if let Ok(report) = guard.report().build() {
    //    let file = File::create("flamegraph.svg").unwrap();
    //    report.flamegraph(file).unwrap();
    //};
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    command: String,
}


fn main() {
    let args = Args::parse();
    match args.command.as_str() {
        "install" => install(),
        _ => {
            println!("This command is not supported yet");
        }
    }
}

// TODO: Update tests
#[cfg(test)]
mod tests {
    use crate::*;
    #[test]
    fn string_parsing_github() {
        println!("Hello");
    }
}
