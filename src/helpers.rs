
use std::fs::File;
use flate2::read::GzDecoder;
use tar::Archive;
use std::path::{Path, PathBuf};
use std::process::Command;
use regex::Regex;
use std::collections::HashMap;
use std::io::{BufRead, BufReader, Read, Write};
use std::fs;
use lazy_static::lazy_static;
use rev_lines::RevLines;

use pathbuf_ext::PathExt;

pub fn download_and_extract(url: &str, folder: String) -> Result<(), Box<dyn std::error::Error>> {

    let folder = if folder.ends_with("/") {
        folder
    } else {
        folder  + "/"
    };

    let response = reqwest::blocking::get(url)?;
    let mut archive = Archive::new(GzDecoder::new(response));
    if !Path::new(&folder).exists() {
        std::fs::create_dir_all(&folder)?;
    }
    // Navigate recursively the tarball and extract all files inside a `data` folder
    for file in archive.entries()? {
        // If the file is inside data folder, extract it
        let mut file = file?;
        let path = file.path()?;
        let path = path.to_str().unwrap();
        if path.contains("data") {
            let outpath: std::path::PathBuf = (folder.clone() + path.split("data/").last().unwrap()).into();
            if let Some(p) = outpath.parent() {
                if !p.exists() {
                    fs::create_dir_all(&p)?;
                }
            }
            file.unpack(&outpath)?;
        }
    }

    // Remove files from folder
    for entry in fs::read_dir(&folder)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            fs::remove_file(path)?;
        }
    }

    Ok(())
}

// Take a command as string and run
pub fn run_command(command: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut command = command.split_whitespace();
    let command_name = command.next().unwrap();
    let command_args = command.collect::<Vec<&str>>();
    let mut command = Command::new(command_name);
    command.args(command_args);
    let output = command.output()?;
    println!("{}", String::from_utf8_lossy(&output.stdout));
    Ok(())
}

// Take a string as argument, then find all `:` in the string that aren't surrounded by `"` nor in lines starting with `--`, then return a vector with all words after :
fn get_method_from_line(line: &str) -> Vec<String> {
    let mut methods: Vec<String> = Vec::new();
    // Go through each character in the line
    let mut current_word = String::new();
    let mut inside_quotes = false;
    let mut inside_comment = false;
    let mut is_method = false;
    let line = line.trim();
    if line.starts_with("function") || line.starts_with("local function") || line.starts_with("--") {
        return methods;
    }
    for c in line.chars() {
        if c == '"' {
            inside_quotes = !inside_quotes;
        }
        if c == '-' && current_word.is_empty() {
            inside_comment = true;
        }
        if c == '\n' {
            inside_comment = false;
            is_method = false;
        }
        if c == ':' && !inside_quotes && !current_word.is_empty() && !inside_comment {
            current_word.clear();
            is_method = true;
        } 
        if (c == '(' || c=='"') && !inside_quotes && !current_word.is_empty() && !inside_comment && is_method {
            methods.push(current_word.clone());
            current_word.clear();
            inside_quotes = false;
            inside_comment = false;
            is_method = false;
        } else {
            current_word.push(c);
        }
    }
    methods
}

lazy_static! {
    static ref RE_FN: Regex = Regex::new(r#"function (\w+):(\w+)\(|^(\w+):(\w+)\("#).unwrap();
    static ref RE_SOUL: Regex = Regex::new(r#"-- SOUL: (.*)"#).unwrap();
    static ref RE_PATH: Regex = Regex::new(r#"("(sprites/|enemies/|entities/|fonts/|items/|maps/|musics/|scripts/|shaders/|sounds/|tilesets/))"#).unwrap();
    static ref RE_DAT: Regex = Regex::new(r#"(src_image = "|tileset = ")"#).unwrap();
    static ref RE_VARS: Regex = Regex::new(r#"local (\w+) = sol.main.get_metatable\(?"(\w+)"\)?"#).unwrap();
    static ref RE_SPRITE: Regex = Regex::new(r#"sprites/(\w+)"#).unwrap();
    static ref RE_CREATE_SPRITE: Regex = Regex::new(r#"create_sprite\(?(?:\w+ or )?"\w+(\/[^"]+")\)?"#).unwrap();
}

pub fn get_new_path(path: &PathBuf, lib_name: &str) -> PathBuf {
    if path.contains("scripts/meta") {
        path.replace("scripts/meta", &("scripts/meta/".to_owned() + &lib_name))
    } else if let Some(captures) = RE_SPRITE.captures(&path.to_str().unwrap()) {
        let sprite_name = captures.get(1).unwrap().as_str().to_string();
        path.replace(&("sprites/".to_owned() + &sprite_name), &("sprites/".to_owned() + &sprite_name + "/" + &lib_name))
    } else {
        let mut new_path = PathBuf::new();
        let path = path.to_str().unwrap().to_string();
        for (i, part) in path.split(|c| c == '/' || c == '\\').enumerate() {
            if i == 2 {
                new_path.push("".to_string() + &lib_name + "/" + part);
            } else {
                new_path.push(part);
            }
        }
        new_path
    }
}

fn get_function_name_from_captures(captures: regex::Captures) -> String {
    let (word1, word2) = {
        if captures.get(1).is_some() {
            (captures.get(1).unwrap().as_str().to_string(), captures.get(2).unwrap().as_str().to_string())
        } else {
            (captures.get(3).unwrap().as_str().to_string(), captures.get(4).unwrap().as_str().to_string())
        }
    };
    word1 + ":" + &word2
}

pub fn get_methods_and_deps(file_name: &PathBuf) -> HashMap<String, Vec<String>> {
    let file = File::open(file_name).expect("Unable to open file");
    let mut methods = HashMap::new();
    let mut declared_methods: Vec<String> = Vec::new();
    let reader = BufReader::new(file);
    let mut current_fn = String::new();
    let mut end_counter = 0;
    for line in reader.lines() {
        // If line is empty or invalid, skip it
        let line = match line {
            Ok(line) => line,
            Err(_) => continue,
        };

        // If the line starts with if, while, for, local function or function, add 1 to end_counter
        if requires_end(&line) && end_counter > 0 {
            end_counter += 1;
        }

        // If the line starts with end, subtract 1 from end_counter
        if is_end(&line) && end_counter > 0 {
            end_counter -= 1;
        }

        // Use the RE_SOUL to get the declared methods
        if let Some(captures) = RE_SOUL.captures(&line) {
            let soul = captures.get(1).unwrap().as_str().to_string();
            let soul_methods = soul.split(",").map(|x| x.to_string()).collect::<Vec<String>>();
            declared_methods.extend(soul_methods);
        }
        
        if let Some(captures) = RE_FN.captures(&line) {
            if end_counter == 0 {
                current_fn = get_function_name_from_captures(captures);
                end_counter += 1;
            }
        }

        // Get all methods from line and extend the current function in the methods HashMap
        let methods_from_line = get_method_from_line(&line);
        if !current_fn.is_empty() {
            methods.entry(current_fn.clone()).or_insert(Vec::new()).extend(methods_from_line.clone());
        }
    }
    methods
}

fn requires_end(line: &str) -> bool {
    let line = line.trim();
    if line.contains("return") {
        return false;
    }
    if line.starts_with("if ") || line.starts_with("while ") || line.starts_with("for ") 
    || line.starts_with("local function ") || line.starts_with("function ") 
    || line.contains("function(")
    {
        return true;
    }
    return false;
}

fn is_end(line: &str) -> bool {
    if line.ends_with("end") || line.ends_with("end,") || line.ends_with("end)") {
        return true;
    }
    return false;
}

fn insert_meta_lines(
    current_line: &mut usize, current_fn_line: &mut usize,
    current_fn_end_line: &mut usize, _current_fn: &str,
    new_file: &mut Vec<String>, files_with_deps: &mut Vec<String>,
    lib_name: &str, _file_name: &PathBuf, _line: &str
) {
    let init_current_line = current_line.clone();
    for file in files_with_deps.clone() {
        let line = "  master_script:set(\"".to_string() + &lib_name + "\", \"" + &(file.split("/").last().unwrap()) + "\")";
        // Check if where the line that is going to be inserted is a return statement
        new_file.insert(*current_fn_line + 1, line);

        let line = "  master_script:set(\"default\", \"".to_string() + &(file.split("/").last().unwrap()) + "\")";

        if new_file[*current_fn_end_line + current_line.clone() - init_current_line].contains("return") {
            new_file.insert(*current_fn_end_line + current_line.clone() - init_current_line, line);
        } else {
            new_file.insert(*current_fn_end_line + current_line.clone() - init_current_line + 1, line);
        }

        *current_line += 2;
    }
    if !new_file[0].contains("master_script = require") && files_with_deps.len() > 0 {
        new_file.insert(0, "local master_script = require(\"scripts/meta/master\")".to_string());
        *current_line += 1;
        *current_fn_line += 1;
        *current_fn_end_line += 1;
    }
}

// Takes a file name, a Hashmap with functions and its' required methods and a Hashmap with files and their methods
// Return a new file where after each function there is a line with the list of files that contain the required methods
pub fn add_deps_to_file(file_name: &PathBuf, methods: &HashMap<String, Vec<String>>, files: &HashMap<String, Vec<String>>, lib_name: String) -> Result<(), Box<dyn std::error::Error>> {
    let file = File::open(file_name).expect("Unable to open file");
    let reader = BufReader::new(file);
    let mut new_file: Vec<String> = Vec::new();
    let mut current_fn = String::new();
    let mut files_with_deps: Vec<String> = Vec::new();
    let mut current_line = 0;
    let mut current_fn_line = 0;
    let mut end_counter = 0;
    let is_meta = file_name.contains("scripts/meta");
    
    let mut current_fn_end_line = 0;
    let mut return_vars = HashMap::new();
    for line in reader.lines() {
    
        let line = match line {
            Ok(line) => line,
            Err(_) => continue,
        };

        if is_meta && line.starts_with("return") && end_counter == 0 {
            current_line += 1;
            continue;
        }
        
        if is_meta && let Some(captures) = RE_VARS.captures(&line) {
            let var_meta = captures.get(1).unwrap().as_str().to_string();
            let var = captures.get(2).unwrap().as_str().to_string();
            return_vars.insert(var, var_meta.clone());
            let new_line = "local ".to_owned() + &var_meta + " = {}";
            new_file.push(new_line);
            current_line += 1;
            continue;
        }

        let line = {
            if let Some(captures) = RE_CREATE_SPRITE.captures(&line) {
                line.clone().replace(
                    &captures.get(1).unwrap().as_str().to_string(), 
                    &(lib_name.clone() + "/" + &(captures.get(1).unwrap().as_str().to_string()))
                )
            } else if file_name.ends_with(".dat") && let Some(captures) = RE_DAT.captures(&line)  {
                let path = captures.get(1).unwrap().as_str().to_string();
                line.clone().replace(&path, &(path.clone() + &lib_name + "/"))
            } else if let Some(captures) = RE_PATH.captures(&line) {
                if line.contains("create_sprite") {
                    line
                } else {
                    let path = captures.get(1).unwrap().as_str().to_string();
                    line.clone().replace(&path, &(path.clone() + &lib_name + "/"))
                }
            } else {
                line
            }
        };


        // If the line starts with if, while, for, local function or function, add 1 to end_counter
        if requires_end(&line) && end_counter > 0 {
            end_counter += 1;
        }
        // If the line starts with end, subtract 1 from end_counter
        if is_end(&line) && end_counter > 0 {
            end_counter -= 1;
            if end_counter == 0 {
                current_fn_end_line = current_line;
            }
        }

        // TODO: Only capture top level function declarations
        if let Some(captures) = RE_FN.captures(&line) {
            if end_counter == 0 {
                if current_fn_line != 0 {
                    // Write `master_script.set(lib_name, file_dep)` to the file
                    insert_meta_lines(
                        &mut current_line, &mut current_fn_line, 
                        &mut current_fn_end_line, &current_fn, 
                        &mut new_file, &mut files_with_deps, 
                        &lib_name, file_name, &line
                    );
                    // For all files in files_with_deps add a line with                ,                                                                               
                    files_with_deps = Vec::new();
                }
                end_counter += 1;
                current_fn = get_function_name_from_captures(captures);
                current_fn_line = current_line;
            }
        }
        if !current_fn.is_empty() {
            if let Some(deps) = methods.get(&current_fn) {
                for dep in deps {
                    for (file, methods) in files {
                        for method in methods {
                            // Test if the method is in the file
                            if (":".to_string() + method.split_once(":").unwrap().1) == dep.to_owned() {
                                files_with_deps.push(file.to_owned());
                            }
                        }
                    }
                }
                files_with_deps.sort();
                files_with_deps.dedup();
            }
        }
        new_file.push(line);
        current_line += 1;
    }
    if files_with_deps.len() > 0 && current_fn_line != 0 {
        insert_meta_lines(
            &mut current_line, &mut current_fn_line, 
            &mut current_fn_end_line, &current_fn, 
            &mut new_file, &mut files_with_deps, 
            &lib_name, file_name, ""
        );
    }

    // If is meta, add at the end of the file a return statement with all meta_vec
    if is_meta {
        for (var, var_meta) in &return_vars {
            new_file.push("local ".to_owned() + &var + " = " + &var_meta);
        }

        let meta_vec = return_vars.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(",");
        new_file.push("return {".to_string() + &meta_vec + "}");
    }

    let new_file = new_file.join("\n");
    std::fs::write(file_name, new_file)?;
    Ok(())
}

// Get a file's blake3
pub fn get_file_blake3(file_name: &PathBuf) -> Result<String, Box<dyn std::error::Error>> {
    let file = File::open(file_name)?;
    let mut reader = BufReader::new(file);
    let mut hasher = blake3::Hasher::new();
    let mut buffer = [0; 1024];
    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        hasher.update(&buffer[..count]);
    }
    let hash = hasher.finalize();
    Ok(hash.to_hex().to_string())
}

pub fn get_files(src_folder: &PathBuf, files: &mut Vec<PathBuf>) -> Result<(), Box<dyn std::error::Error>> {
    let paths = fs::read_dir(src_folder)?;
    for path in paths {
        let path = path?.path();
        if !path.is_file() {
            get_files(&path, files)?;
        } else {
            files.push(path);
        }
    }
    Ok(())
}


static TEMPLATE: &'static str = include_str!("data/pm.lua");
pub fn generate_master_script(src_folder: &PathBuf, dest: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {
    let mut files: Vec<PathBuf> = Vec::new();
    get_files(src_folder, &mut files)?;
    let mut master_script = String::new();
    let mut list_of_vars = Vec::new();

    for file in files {
        if file.contains("scripts/meta") {
            let file2 = file.file_name().unwrap().to_str().unwrap().to_string().replace(".lua", "");

            // Read file from bottom to top, declare return_line when you find return {
            let file = File::open(file).unwrap();
            let rev_lines = RevLines::new(file);
            let mut return_line = String::new();

            for line in rev_lines {
                match line {
                    Ok(line) => {
                        if line.contains("return {") {
                            return_line = line;
                            break;
                        }
                    },
                    Err(_) => {
                        continue
                    }
                };
            }

            // Skip if return_line is empty
            if return_line.is_empty() || list_of_vars.contains(&file2) {
                continue;
            }

            let return_line = return_line.split_once("{").unwrap().1;
            let return_line = return_line.split_once("}").unwrap().0;
            let return_line = return_line.split(",").collect::<Vec<&str>>();

            master_script += &("".to_owned() + &file2 + " = {\n");
            master_script += "\tcurrent = \"default\",\n";
            master_script += "\tdefault = {\n";
            for capture in &return_line {
                master_script += &("\t\t".to_owned() + "deepcopy(sol.main.get_metatable(\"" + capture + "\")),\n");
            }
            master_script += "\t},\n";
            master_script += "\ttables = {\n";
            for capture in &return_line {
                master_script += &("\t\t".to_owned() + "sol.main.get_metatable(\"" + capture + "\"),\n");
            }
            master_script += "\t},\n";
            master_script += "},\n";

            list_of_vars.push(file2);
        }
    }

    let mut file = File::create(dest.join("scripts/meta/master.lua"))?;
    file.write_all(TEMPLATE.replace("--REPLACE--", &master_script).as_bytes())?;
    Ok(())
}

